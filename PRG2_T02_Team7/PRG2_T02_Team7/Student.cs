﻿//============================================================
// Student Number : S10223182, S10219353
// Student Name : Gwendolyn Leong, Gabriel Koh
// Module Group : T02
//============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG2_T02_Team7
{
    class Student : Ticket
    {
        //levelOfStudy:string
        public string LevelOfStudy { get; set; }
        //Student()
        public Student() { }
        //Student(Screening, string) *
        public Student(Screening screening, string lvlOfStudy) : base(screening)
        {
            LevelOfStudy = lvlOfStudy;
        }
        //CalculatePrice():double *
        public override double CalculatePrice()
        {
            string stringDateTime = Screening.ScreeningDateTime.ToString("ddd");
            if (Screening.ScreeningType == "3D")
            {
                if (stringDateTime == "Mon" || stringDateTime == "Tue" || stringDateTime == "Wed" || stringDateTime == "Thu")
                {
                    return 8;
                }
                else
                {
                    return 14;
                }
            }
            else
            {
                if (stringDateTime == "Mon" || stringDateTime == "Tue" || stringDateTime == "Wed" || stringDateTime == "Thu")
                {
                    return 7;
                }
                else
                {
                    return 12.5;
                }
            }
        }
        //ToString():string 
        public override string ToString()
        {
            return "Total price of student ticket: " + CalculatePrice();
        }
    }
}
