﻿//============================================================
// Student Number : S10223182, S10219353
// Student Name : Gwendolyn Leong, Gabriel Koh
// Module Group : T02
//============================================================

using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace PRG2_T02_Team7
{
    class Program
    {
        //global variables
        public static List<Movie> Movies;
        public static List<Cinema> Cinemas;
        public static List<Screening> Screenings;
        public static List<Order> Orders = new List<Order>();
        public static void Main(string[] args)
        {
            //Load movie, cinema, screening data
            Movies = LoadMovieData();
            Cinemas = LoadCinemaData();
            Screenings = LoadScreening(Cinemas, Movies);

            while (true)
            {
                DisplayMenu();
                Console.Write("Enter input: ");
                string input = Console.ReadLine();

                //list all movies
                if (input == "1")
                {
                    DisplayMovieDetails(Movies);
                }
                //list movie screening
                else if (input == "2")
                {
                    DisplayScreeningForMovie();
                }
                //add movie screening session
                else if (input == "3")
                {
                    AddingMovieScreeningSession();
                }
                //delete movie screening session
                else if (input == "4")
                {
                    DeletingMovieScreeningSession();
                }
                //order movie ticket
                else if (input == "5")
                {
                    OrderingMovieTickets();
                }
                //cancel order of ticket
                else if (input == "6")
                {
                    CancellingOrder();
                }
                //recommend movie based on sale of tickets sold
                else if (input == "7")
                {
                    RecommandMovie();
                }
                //display available seats of screening session in descending order
                else if (input == "8")
                {
                    DisplayAvailableSeats();
                }
                //exit program
                else if (input == "9")
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Invalid input, try again.");
                }
                Console.WriteLine();
            }
            Console.WriteLine("Application closed.");
        }
        // main menu display
        static void DisplayMenu()
        {
            Console.WriteLine("=========MAIN MENU=========");
            Console.WriteLine("1) List all movies");
            Console.WriteLine("2) List movie screenings");
            Console.WriteLine("3) Add a movie screening session");
            Console.WriteLine("4) Delete a movie screening session");
            Console.WriteLine("5) Order movie tickets");
            Console.WriteLine("6) Cancel order of ticket");
            Console.WriteLine("7) Recommend movie based on sale of tickets sold");
            Console.WriteLine("8) Display available seats of screening session in descending order");
            Console.WriteLine("9) EXIT");
        }

        //1) load movie data from csv file and populate into a list (Gwendolyn)
        static List<Movie> LoadMovieData()
        {
            List<Movie> movies = new List<Movie>();
            string[] csvLines = File.ReadAllLines("Movie.csv");
            for (int i = 1; i < csvLines.Length; i++)
            {
                string[] dataArray = csvLines[i].Split(',');
                string title = dataArray[0];

                // check if title is empty, if yes then skip
                if (title.Length == 0) continue;

                int duration = Convert.ToInt32(dataArray[1]);
                string genreString = dataArray[2];
                string classification = dataArray[3];
                DateTime openingDate = Convert.ToDateTime(dataArray[4]);
                //add new input
                List<string> genreList = new List<string>();
                Movie m = new Movie(title, duration, classification, openingDate, genreList);
                m.AddGenre(genreString);
                movies.Add(m);
            }
            return movies;
        }

        //1) load cinema data from csv file and populate into a list (Gwendolyn)
        static List<Cinema> LoadCinemaData()
        {
            List<Cinema> cinemas = new List<Cinema>();
            string[] csvLines = File.ReadAllLines("Cinema.csv");
            for (int i = 1; i < csvLines.Length; i++)
            {
                string[] dataArray = csvLines[i].Split(',');
                string name = dataArray[0];
                int hallNumber = Convert.ToInt32(dataArray[1]);
                int capacity = Convert.ToInt32(dataArray[2]);
                //add new input
                Cinema c = new Cinema(name, hallNumber, capacity);
                cinemas.Add(c);
            }
            return cinemas;
        }

        //2) method to load screening data from csv file and populate into list (Gwendolyn)
        static List<Screening> LoadScreening(List<Cinema> cinemas, List<Movie> movies)
        {
            List<Screening> screenings = new List<Screening>();
            string[] csvLines = File.ReadAllLines("Screening.csv");
            int screeningNumber = 1001;
            for (int i = 1; i < csvLines.Length; i++)
            {
                string[] dataArray = csvLines[i].Split(',');
                string dateTimeString = dataArray[0];

                if (dateTimeString.Length == 0) continue;

                DateTime dateTime = Convert.ToDateTime(dateTimeString);
                string screeningType = dataArray[1];
                string cinemaName = dataArray[2];
                int hallNumber = Convert.ToInt32(dataArray[3]);
                string movieTitle = dataArray[4];

                Cinema cinema = SearchCinemaByNameAndHall(cinemas, cinemaName, hallNumber);
                Movie movie = SearchMovieByTitle(movies, movieTitle);

                Screening screening = new Screening(screeningNumber, dateTime, screeningType, cinema, movie);
                screenings.Add(screening);
                screeningNumber++;
            }
            return screenings;
        }

        //3) list all movies and its information (Gwendolyn)
        static void DisplayMovieDetails(List<Movie> movieObjects)
        {
            Console.WriteLine("{0,-25} {1,-15} {2,-25} {3,-20} {4,-20}", "Title", "Duration(mins)", "Genre", "Classification", "Opening Date");
            Console.WriteLine("{0,-25} {1,-15} {2,-25} {3,-20} {4,-20}", "-----", "--------------", "-----", "--------------", "------------");
            foreach (Movie m in movieObjects)
            {
                Console.WriteLine(m);

            }
            Console.WriteLine();
        }

        //print cinema list
        static void DisplayCinemaDetails(List<Cinema> cinemaObjects)
        {
            Console.WriteLine("{0,-15} {1,-15} {2,-20}", "Name", "Hall Number", "Capacity");
            Console.WriteLine("{0,-15} {1,-15} {2,-20}", "----", "-----------", "--------");
            foreach (Cinema c in cinemaObjects)
            {
                Console.WriteLine(c);
            }
        }

        //method to print screening list
        static void DisplayScreeningDetails(List<Screening> screenings)
        {
            Console.WriteLine("{0,-15} {1,-25} {2,-20} {3,-20} {4,-15} {5,-10} {6,-10}", "Screening No.", "Date Time", "Screening Type", "Seats Remaining", "Cinema", "Hall No.", "Movie Title");
            Console.WriteLine("{0,-15} {1,-25} {2,-20} {3,-20} {4,-15} {5,-10} {6,-10}", "-------------", "---------", "--------------", "---------------", "------", "--------", "-----------");
            foreach (Screening screening in screenings)
            {
                Console.WriteLine(screening);
            }
            Console.WriteLine();
        }

        //print order details list
        static void DisplayOrderDetails(List<Order> orders, bool onlyAvailable = false)
        {
            Console.WriteLine("{0,-30} {1,-30} {2,-30} {3,-30}", "Order No", "Order Datetime", "Amount($)", "Status");
            Console.WriteLine("{0,-30} {1,-30} {2,-30} {3,-30}", "--------", "--------------", "---------", "------");
            foreach (Order o in orders)
            {
                if (onlyAvailable)
                {
                    if (o.Status != "Cancelled")
                    {
                        Console.WriteLine(o);
                    }
                }
                else
                {
                    Console.WriteLine(o);
                }
            }
            Console.WriteLine();
        }

        //method to search a title from movie list
        static Movie SearchMovieByTitle(List<Movie> moviesToSearch, string movieTitle)
        {
            foreach (Movie movie in moviesToSearch)
            {
                if (movie.Title.ToLower() == movieTitle.ToLower())
                {
                    return movie;
                }

            }
            return null;
        }

        //method to search a name and hall from cinema list
        static Cinema SearchCinemaByNameAndHall(List<Cinema> cinemasToSearch, string cinemaName, int hallNo)
        {
            foreach (Cinema cinema in cinemasToSearch)
            {
                if (cinema.Name.ToLower() == cinemaName.ToLower() && cinema.HallNo == hallNo)
                {
                    return cinema;
                }

            }
            return null;
        }

        //method to search a cinema hall from screening list
        static List<Screening> SearchScreeningsByCinemaHall(Cinema cinema)
        {
            List<Screening> screenings = new List<Screening>();
            foreach (Screening screening in Screenings)
            {
                if (screening.Cinema == cinema)
                {
                    screenings.Add(screening);
                }
            }
            return screenings;
        }

        //method to search screening number from screening list
        static Screening SearchScreeningByNumber(int screeningNo)
        {
            foreach (Screening screening in Screenings)
            {
                if (screening.ScreeningNo == screeningNo)
                {
                    return screening;
                }
            }
            return null;
        }

        //method to search screening number from updated screening list
        static Screening SearchScreeningByNumber(List<Screening> screenings,int screeningNo)
        {
            foreach (Screening screening in Screenings)
            {
                if (screening.ScreeningNo == screeningNo)
                {
                    return screening;
                }
            }
            return null;
        }

        //method to search order number from order
        static Order SearchOrderByOrderNo(List<Order>orders, int orderNo)
        {
            foreach (Order order in orders)
            {
                if (order.OrderNo == orderNo)
                {
                    return order;
                }
            }
            return null;
        }

        //method to search a movie from screening list
        static List<Screening> SearchScreeningByMovie(Movie movie)
        {
            List<Screening> screenings = new List<Screening>();
            foreach (Screening screening in Screenings)
            {
                if (screening.Movie == movie)
                {
                    screenings.Add(screening);
                }
            }
            return screenings;
        }

        // 4) list movie screenings (Gwendolyn)
        static void DisplayScreeningForMovie()
        {
            Movie movieFound;
            //prompt user to select movie
            while (true)
            {
                //list all movies
                DisplayMovieDetails(Movies);
                Console.Write("Select a movie (enter to exit): ");
                string movieSelected = Console.ReadLine();
                if (movieSelected == "")
                {
                    movieFound = null;
                    break;
                }
                movieFound = SearchMovieByTitle(Movies, movieSelected);
                if (movieFound == null)
                {
                    Console.WriteLine("Movie not found, please try again.");
                }
                else
                {
                    break;
                }
            }
            if (movieFound != null)
            {
                List<Screening> screenings = SearchScreeningByMovie(movieFound);
                DisplayScreeningDetails(screenings);
            }

        }

        //5) adding a movie screening session (Gwendolyn and Gabriel)
        static void AddingMovieScreeningSession()
        {
            Movie movieFound;
            string screeningTypeSelected;
            DateTime datetimeSelected;
            while (true)
            {
                //list all movies
                DisplayMovieDetails(Movies);
                Console.Write("Select a movie: ");
                string movieSelected = Console.ReadLine();
                if (movieSelected == "")
                {
                    return;
                }
                movieFound = SearchMovieByTitle(Movies, movieSelected);
                if (movieFound == null)
                {
                    Console.WriteLine("Movie not found, try again.");
                }
                else
                {
                    break;
                }
            }
            //prompt user to enter a screening type
            while (true)
            {
                Console.Write("Enter a screening type (2D / 3D): ");
                screeningTypeSelected = Console.ReadLine();
                if (screeningTypeSelected != "2D" && screeningTypeSelected != "3D")
                {
                    Console.WriteLine("Invalid screening type, please try again.");
                }
                else
                {
                    break;
                }
            }
            //prompt user to enter a screening date and time
            while (true)
            {
                Console.Write("Enter a screening date and time (eg. 31/01/2022 8:00PM): ");
                string datetimeString = Console.ReadLine();
                //try convert datetime string
                try
                {
                    datetimeSelected = Convert.ToDateTime(datetimeString);
                }
                catch (FormatException)
                {
                    Console.WriteLine("Invalid datetime format, please try again. (eg. 31/01/2022 8:00PM)");
                    continue;
                }
                //check if datetime is after opening date of movie
                int comparisonResult = DateTime.Compare(datetimeSelected, movieFound.OpeningDate);
                if (comparisonResult < 0)
                {
                    //if earlier than movie opening date
                    Console.WriteLine("Screening datetime is earlier than movie opening date ({0}). Please try again.", movieFound.OpeningDate);
                    continue;
                }
                break;
            }
            //list all cinema halls
            DisplayCinemaDetails(Cinemas);
            Cinema cinemaSelected;
            //prompt the user to select a cinema hall
            while (true)
            {
                Console.Write("Enter a cinema name: ");
                string cinemaName = Console.ReadLine();
                Console.Write("Enter a hall number: ");
                string hallNoString = Console.ReadLine();
                //Check if hall no is a valid int
                try
                {
                    int hallNo = Convert.ToInt32(hallNoString);
                    cinemaSelected = SearchCinemaByNameAndHall(Cinemas, cinemaName, hallNo);
                    if (cinemaSelected == null)
                    {
                        Console.WriteLine("Cinema hall not found");
                        DisplayCinemaDetails(Cinemas);
                        continue;
                    }
                    else
                    {
                        //validate selected hall
                        //calculate the end datetime of selected screening
                        DateTime endDateTimeOfSelectedScreening = datetimeSelected.AddMinutes(movieFound.Duration).AddMinutes(30);
                        //get a list of existing screenings for the selected cinema hall
                        List<Screening> existingScreenings = SearchScreeningsByCinemaHall(cinemaSelected);
                        //for each screening, validate
                        foreach (Screening screening in existingScreenings)
                        {
                            DateTime endDateTimeOfExistingScreening = screening.ScreeningDateTime.AddMinutes(screening.Movie.Duration);

                            //does not clash with the screening
                            if (endDateTimeOfSelectedScreening.CompareTo(screening.ScreeningDateTime) < 0 || datetimeSelected.CompareTo(endDateTimeOfExistingScreening) > 0)
                            {
                                continue;
                            }
                            //unsuccessful
                            else
                            {
                                Console.WriteLine("Selected screening time clashes with existing screenings. Please try again.");
                                return;
                            }
                        }
                        //does not clash with any of the screenings
                        Screening newScreening = new Screening(1001 + Screenings.Count, datetimeSelected, screeningTypeSelected, cinemaSelected, movieFound);
                        Screenings.Add(newScreening);
                        Console.WriteLine("New screening is added successfully!");
                        return;
                    }
                }
                catch (Exception)
                {
                    Console.Write("Please enter a valid integer for hall number, please try again.");
                    continue;
                }
            }

        }

        //6) delete a movie screening session (Gwendolyn and Gabriel)
        static void DeletingMovieScreeningSession()
        {
            //filter screenings with 0 tickets sold
            List<Screening> screeningsWithZeroTicketsSold = new List<Screening>();

            foreach (Screening screening in Screenings)
            {
                if (screening.SeatsRemaining == screening.Cinema.Capacity)
                {
                    screeningsWithZeroTicketsSold.Add(screening);
                }
            }
            DisplayScreeningDetails(screeningsWithZeroTicketsSold);
            Screening selectedScreening;
            while (true)
            {
                //prompt user to select a session
                Console.Write("Select a session (eg. 1001, press enter to exit): ");
                string sessionSelectedString = Console.ReadLine();
                if (sessionSelectedString == "")
                {
                    return;
                }
                try
                {
                    int screeningNoSelected = Convert.ToInt32(sessionSelectedString);
                    selectedScreening = SearchScreeningByNumber(screeningNoSelected);
                    if (selectedScreening != null)
                    {
                        break;
                    }
                    else
                    {
                        Console.WriteLine("No matching screening found. please try again.");
                        DisplayScreeningDetails(screeningsWithZeroTicketsSold);
                        continue;
                    }
                }
                catch (Exception)
                {
                    Console.WriteLine("Invalid screening No. Please try again.");
                    continue;
                }
            }
            //delete the screening
            if (Screenings.Remove(selectedScreening))
            {
                Console.WriteLine("Removal successful!");
            }
            else
            {
                Console.WriteLine("Removal unsuccessful. try again");
            }
        }

        //7) order movie tickets (Gwendolyn)
        static void OrderingMovieTickets()
        {
            Movie movieSelected;
            while (true)
            {
                //list of all movies
                DisplayMovieDetails(Movies);
                //prompt user to select movie
                Console.Write("Select a movie by title: ");
                string selectedMovieString = Console.ReadLine();
                movieSelected = SearchMovieByTitle(Movies, selectedMovieString);
                if (movieSelected == null)
                {
                    Console.WriteLine("Movie not found, please try again.");
                    continue;
                }
                else
                {
                    break;
                }
            }
            List<Screening> screeningsAvailable = SearchScreeningByMovie(movieSelected);
            List<Screening> filteredScreeningByDateTime = new List<Screening>();
            foreach (Screening screening in screeningsAvailable)
            {
                //filter screenings that are not over yet
                DateTime endDatetime = screening.ScreeningDateTime.AddMinutes(screening.Movie.Duration).AddMinutes(30);
                if (endDatetime.CompareTo(DateTime.Now) > 0)
                {
                    filteredScreeningByDateTime.Add(screening);
                }
            }
            //if there are no screenings for movie
            if (screeningsAvailable.Count == 0)
            {
                Console.WriteLine("No available screenings for the selected movie.");
                return;
            }
            DisplayScreeningDetails(screeningsAvailable);
            Screening selectedScreening;
            //retrieve selected movie screening
            while (true)
            {

                Console.Write("Select a session (eg. 1001): ");
                string selectedScreeningNo = Console.ReadLine();

                try
                {
                    int screeningNoSelected = Convert.ToInt32(selectedScreeningNo);
                    selectedScreening = SearchScreeningByNumber(screeningsAvailable, screeningNoSelected);
                    if (selectedScreening != null)
                    {
                        break;
                    }
                    else
                    {
                        Console.WriteLine("No matching screening found, please try again. \n");
                        DisplayScreeningDetails(screeningsAvailable);
                        continue;
                    }
                }
                catch (Exception)
                {
                    Console.WriteLine("Please enter valid screening No. (eg. 1001).");
                    continue;
                }
            }
            //prompt the user to enter number of tickets to order
            int ticketQuantity;
            while (true)
            {
                Console.Write("Enter the total number of tickets to order: ");
                string ticketQuantityString = Console.ReadLine();
                try
                {
                    ticketQuantity = Convert.ToInt32(ticketQuantityString);
                    int cinemaCapacity = selectedScreening.Cinema.Capacity;
                    if (ticketQuantity <= 0)
                    {
                        Console.WriteLine("Please enter a positive integer, please try again.");
                        continue;
                    }
                    else if (ticketQuantity > cinemaCapacity)
                    {
                        Console.WriteLine("Cinema capacity is {0}. Please enter an integer less than this amount.",cinemaCapacity);
                        continue;
                    }
                    else
                    {
                        break;
                    }
                }
                catch (Exception)
                {
                    Console.WriteLine("Please enter valid integer, please try again.");
                    continue;
                }
            }
            //check if movie selected is not G
            if (movieSelected.Classification != "G")
            {
                while (true)
                {
                    //prompt user for classification requirement confirmation
                    Console.Write("The movie classification is {0}.Do you meet the movie classification requirements? [Y/N]: ", movieSelected.Classification);
                    string response = Console.ReadLine();
                    if (response.ToLower() == "n")
                    {
                        return;
                    }
                    else if (response.ToLower() == "y")
                    {
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Please enter valid option [Y/N]: ");
                        continue;
                    }
                }

            }
            //create an Order object with the status “Unpaid”
            Order order = new Order(Orders.Count + 1, DateTime.Now);
            // for each ticket
            for (int i = 0; i < ticketQuantity; i++)
            {
                //prompt user for ticket type
                string ticketTypeSelected;
                while (true)
                {
                    Console.Write("Select a ticket type [Student/Senior Citizen/Adult]: ");
                    string[] ticketTypes = { "student", "senior citizen", "adult" };
                    ticketTypeSelected = Console.ReadLine();
                    if (Array.IndexOf(ticketTypes, ticketTypeSelected.ToLower()) == -1)
                    {
                        Console.WriteLine("Invalid ticket type, please try again. [Student/Senior Citizen/Adult]: ");
                        continue;
                    }
                    break;
                }
                if (ticketTypeSelected.ToLower() == "student")
                {
                    string levelOfStudySelected;
                    while (true)
                    {
                        Console.Write("Enter level of study [Primary/Secondary/Tertiary]: ");
                        string[] levelOfStudies = { "primary", "secondary", "tertiary" };
                        levelOfStudySelected = Console.ReadLine();
                        if (Array.IndexOf(levelOfStudies, levelOfStudySelected.ToLower()) == -1)
                        {
                            Console.WriteLine("Invalid level of study, please try again. [Primary/Secondary/Tertiary]: ");
                            continue;
                        }
                        break;
                    }
                    //create a student ticket
                    Student studentTicket = new Student(selectedScreening, levelOfStudySelected);
                    //add the ticket to order 
                    order.AddTicket(studentTicket);
                    //update seats remaining for the screening
                    selectedScreening.SeatsRemaining--;
                }
                else if (ticketTypeSelected.ToLower() == "senior citizen")
                {
                    int yearOfBirth;
                    while (true)
                    {
                        Console.Write("Enter your year of birth: ");
                        string yobString = Console.ReadLine();
                        try
                        {
                            yearOfBirth = Convert.ToInt32(yobString);
                            int age = DateTime.Now.Year - yearOfBirth;
                            if (age < 55)
                            {
                                Console.WriteLine("You are not qualified as a senior citizen.");
                                i--;
                                continue;
                            }
                            else
                            {
                                break;
                            }
                        }
                        catch (Exception)
                        {
                            Console.WriteLine("Please enter valid year.");
                            continue;
                        }
                    }
                    //create senior citizen ticket
                    SeniorCitizen seniorCitizenTicket = new SeniorCitizen(selectedScreening, yearOfBirth);
                    //add the ticket to order 
                    order.AddTicket(seniorCitizenTicket);
                    //update seats remaining for the screening
                    selectedScreening.SeatsRemaining--;
                }
                else if (ticketTypeSelected.ToLower() == "adult")
                {
                    bool wantsPopcorn;
                    while (true)
                    {
                        Console.Write("Do you want popcorn? [Y/N]: ");
                        string response = Console.ReadLine();
                        if (response.ToLower() == "n")
                        {
                            wantsPopcorn = false;
                        }
                        else if (response.ToLower() == "y")
                        {
                            wantsPopcorn = true;
                        }
                        else
                        {
                            Console.WriteLine("Please enter valid option [Y/N].");
                            continue;
                        }
                        break;
                    }
                    //create adult ticket
                    Adult adultTicket = new Adult(selectedScreening, wantsPopcorn);
                    //add the ticket to order 
                    order.AddTicket(adultTicket);
                    //update seats remaining for the screening
                    selectedScreening.SeatsRemaining--;

                }
            }
            //calculate total amount payable 
            bool applyAdultPrice = false;
            //check if order is within first 7 days of movie opening date
            DateTime endingDateTimeIn7Days = selectedScreening.Movie.OpeningDate.AddDays(7);
            if (order.OrderDateTime.CompareTo(endingDateTimeIn7Days) <= 0 && order.OrderDateTime.CompareTo(selectedScreening.Movie.OpeningDate) >= 0)
            {
                applyAdultPrice = true;
            }
            double totalPricePayable = 0;
            foreach (Ticket ticket in order.TicketList)
            {
                double price;
                if (applyAdultPrice && ticket.GetType() != typeof(Adult))
                {
                    price = new Adult(selectedScreening, false).CalculatePrice(); ;
                }
                else
                {
                    price = ticket.CalculatePrice();
                }
                totalPricePayable += price;
            }
            //prompt for payment
            Console.Write("Please press any key to make payment: ");
            Console.ReadLine();

            order.Amount = totalPricePayable;
            order.Status = "Paid";
            Orders.Add(order);
            Console.WriteLine("Payment ${0} sucessful!", order.Amount);
            
        }
        //8) cancel order of ticket (Gwendolyn)
        static void CancellingOrder()
        {
            List<Order> availableOrders = new List<Order>();
            foreach (Order eachOrder in Orders)
            {
                if (eachOrder.Status != "Cancelled")
                {
                    availableOrders.Add(eachOrder);
                }
            }
            if (availableOrders.Count == 0)
            {
                Console.WriteLine("No available orders.");
                return;
            }
            DisplayOrderDetails(availableOrders);
            Order order;
            while (true)
            {
                Console.Write("Enter order number to cancel: ");
                string orderNoString = Console.ReadLine();
                //try convert order no string
                try
                {
                    int orderNoSelected = Convert.ToInt32(orderNoString);
                    order = SearchOrderByOrderNo(availableOrders, orderNoSelected);
                    if (order == null)
                    {
                        Console.WriteLine("Order not found, please try again.");
                        DisplayOrderDetails(availableOrders);
                        continue;
                    }
                    break;
                }
                catch (Exception)
                {
                    Console.WriteLine("Invalid order number, please try again.");
                    continue;
                }
            }
            Screening screening = order.TicketList[0].Screening;
            DateTime endDatetime = screening.ScreeningDateTime.AddMinutes(screening.Movie.Duration).AddMinutes(30);

            //check if screening has started
            if (screening.ScreeningDateTime.CompareTo(DateTime.Now) > 0)
            {
                Console.WriteLine("The screening of the order has started and it cannot be cancelled.");
                return;
            }
            //cancel the order
            screening.SeatsRemaining += order.TicketList.Count;
            order.Status = "Cancelled";
            Console.WriteLine("The order amount has been refunded.");
            Console.WriteLine("The order is cancelled successfully");

        }

        //3.1 Recommend movie based on sale of tickets sold (Gwendolyn)
        static void RecommandMovie()
        {
            List<Movie> moviesRecommendationList = new List<Movie>();
            List<int> ticketsSoldByMovies = new List<int>();
            foreach (Order order in Orders)
            {
                if (order.Status == "Paid")
                {
                    List<Ticket> tickets = order.TicketList;
                    int ticketCount = tickets.Count;
                    Screening screening = tickets[0].Screening;
                    Movie movie = screening.Movie;

                    int indexOfMovie = Array.IndexOf(moviesRecommendationList.ToArray(), movie);
                    if (indexOfMovie == -1)
                    {
                        moviesRecommendationList.Add(movie);
                        ticketsSoldByMovies.Add(ticketCount);
                    }
                    else
                    {
                        ticketsSoldByMovies[indexOfMovie] += ticketCount;
                    }
                }
            }
            //if there are no tickets sold
            if (moviesRecommendationList.Count == 0)
            {
                Console.WriteLine("No tickets have been sold.");
                return;
            }
            List<Tuple<Movie, int>> combineMovieTicketCount = new List<Tuple<Movie, int>>();
            for (int i = 0; i < moviesRecommendationList.Count; i++)
            {
                Movie movie = moviesRecommendationList[i];
                int ticketSold = ticketsSoldByMovies[i]; ;
                combineMovieTicketCount.Add(new Tuple<Movie, int>(movie, ticketSold));
            }
            //sort tuple in descending order based on ticket 

            //if return > 0, element 2 is before element1
            //if return < 0, element 2 is after element1
            //if return == 0, follow original order
            combineMovieTicketCount.Sort(
                (element1, element2) => { return element2.Item2 - element1.Item2; });
            int top = 3;
            if (combineMovieTicketCount.Count < 3)
            {
                top = combineMovieTicketCount.Count;
            }
            //get top 3
            Console.WriteLine("{0,-25} {1,-15} {2,-25} {3,-20} {4,-25} {5,-20}", "Title", "Duration(mins)", "Genre", "Classification", "Opening Date", "Tickets sold");
            Console.WriteLine("{0,-25} {1,-15} {2,-25} {3,-20} {4,-25} {5,-20}", "-----", "--------------", "-----", "--------------", "------------", "------------");
            for (int i = 0; i < top; i++)
            {
                Movie movie = combineMovieTicketCount[i].Item1;
                int ticketsSold = combineMovieTicketCount[i].Item2;
                Console.WriteLine("{0,-25} {1,-15} {2,-25} {3,-20} {4,-25} {5,-20}", movie.Title, movie.Duration, string.Join('/', movie.GenreList), movie.Classification, movie.OpeningDate, ticketsSold);
            }
        }

        //3.2 Display available seats of screening session in descending order (Gabriel)
        static void DisplayAvailableSeats()
        {
            List<Screening> screeningsWithTicketsSold = new List<Screening>();
            foreach (Screening screening in Screenings)
            {
                screeningsWithTicketsSold.Add(screening);
            }
            //sorting list in descending order
            screeningsWithTicketsSold = screeningsWithTicketsSold.OrderByDescending(screening => screening.SeatsRemaining).ToList();
            DisplayScreeningDetails(screeningsWithTicketsSold);
        }
    }
}
