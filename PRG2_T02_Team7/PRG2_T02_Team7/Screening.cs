﻿//============================================================
// Student Number : S10223182, S10219353
// Student Name : Gwendolyn Leong, Gabriel Koh
// Module Group : T02
//============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG2_T02_Team7
{
    class Screening
    {
        private int screeningNo;
        private DateTime screeningDateTime;
        private string screeningType;
        private int seatsRemaining;
        private Cinema cinema;
        private Movie movie;

        public int ScreeningNo { get; set; }
        public DateTime ScreeningDateTime { get; set; }
        public string ScreeningType { get; set; }
        public int SeatsRemaining { get; set; }
        public Cinema Cinema { get; set; }
        public Movie Movie { get; set; }
        //Screening()
        public Screening() { }
        //Screening(int,DateTime,string,Cinema,Movie)
        public Screening(int screeningNo, DateTime screeningDateTime, string screeningType, Cinema cinema, Movie movie)
        {
            ScreeningNo = screeningNo;
            ScreeningDateTime = screeningDateTime;
            ScreeningType = screeningType;
            Cinema = cinema;
            Movie = movie;
            //update seats in screening
            SeatsRemaining = cinema.Capacity;
        }
        //ToString():string *
        public override string ToString()
        {
            return string.Format("{0,-15} {1,-25} {2,-20} {3,-20} {4,-15} {5,-10} {6,-10}", ScreeningNo, ScreeningDateTime, ScreeningType, SeatsRemaining, Cinema.Name, Cinema.HallNo, Movie.Title);
        }
    }
}
