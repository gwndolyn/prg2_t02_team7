﻿//============================================================
// Student Number : S10223182, S10219353
// Student Name : Gwendolyn Leong, Gabriel Koh
// Module Group : T02
//============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG2_T02_Team7
{
    class Movie
    {
        private string title;
        private int duration;
        private string classification;
        private DateTime openingDate;
        private List<string> genreList;
        private List<Screening> screeningList;

        public string Title { get; set; }
        public int Duration { get; set; }
        public string Classification { get; set; }
        public DateTime OpeningDate { get; set; }
        public List<string> GenreList { get; set; } 
        public List<Screening> ScreeningList { get; set; } = new List<Screening>();

        public Movie() { }
        public Movie(string title,int duration, string classification, DateTime openingDate, List<string> genreList)
        {
            Title = title;
            Duration = duration;
            Classification = classification;
            OpeningDate = openingDate;
            GenreList = genreList;
        }
        public void AddGenre(string title)
        {
            // split genres into list
            string[] genres = title.Split("/");
            foreach (string genre in genres)
            {
                GenreList.Add(genre);
            }
        }
        public void AddScreening(Screening screen)
        {
            ScreeningList.Add(screen);
        }
        public override string ToString()
        {
            return string.Format("{0,-25} {1,-15} {2,-25} {3,-20} {4,-20}", Title, Duration, string.Join('/', GenreList), Classification, OpeningDate);
        }
    }
}
