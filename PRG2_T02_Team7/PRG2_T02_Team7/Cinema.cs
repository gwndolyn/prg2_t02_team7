﻿//============================================================
// Student Number : S10223182, S10219353
// Student Name : Gwendolyn Leong, Gabriel Koh
// Module Group : T02
//============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG2_T02_Team7
{
    class Cinema
    {
        private string name;
        private int hallNo;
        private int capacity;
        public string Name { get; set; }
        public int HallNo { get; set; }
        public int Capacity { get; set; }

        public Cinema() { }
        public Cinema(string name, int hallno, int cap) 
        {
            Name = name;
            HallNo = hallno;
            Capacity = cap;
        }
        public override string ToString()
        {
            return string.Format("{0,-15} {1,-15} {2,-20}", Name, HallNo, Capacity);
        }
    }
}
