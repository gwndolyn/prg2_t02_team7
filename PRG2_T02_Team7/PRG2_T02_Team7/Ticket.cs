﻿//============================================================
// Student Number : S10223182, S10219353
// Student Name : Gwendolyn Leong, Gabriel Koh
// Module Group : T02
//============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG2_T02_Team7
{
    abstract class Ticket
    {
        //screening:Screening
        public Screening Screening { get; set; }

        //Ticket()
        public Ticket() { }
        //Ticket(Screening)
        public Ticket(Screening screening)
        {
            Screening = screening;
        }
        //CalculatePrice():double *
        public abstract double CalculatePrice();
        //ToString():String *
        public override string ToString()
        {
            return "Screening: " + Screening;
        }


    }
}
