﻿//============================================================
// Student Number : S10223182, S10219353
// Student Name : Gwendolyn Leong, Gabriel Koh
// Module Group : T02
//============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG2_T02_Team7
{
    class Adult : Ticket
    {
        //popcornOffer:bool
        public bool PopcornOffer { get; set; }
        //Adult()
        public Adult() { }
        //Adult(Screening,bool)
        public Adult(Screening screening, bool popcornOffer) : base(screening)
        {
            PopcornOffer = popcornOffer;
        }
        //CalculatePrice():double *
        public override double CalculatePrice()
        {
            double popcornCost = 0;
            if (PopcornOffer)
            {
                popcornCost = 3;
            }
            string stringDateTime = Screening.ScreeningDateTime.ToString("ddd");
            if (Screening.ScreeningType == "3D")
            {
                if (stringDateTime == "Mon" || stringDateTime == "Tue" || stringDateTime == "Wed" || stringDateTime == "Thu")
                {
                    return 11 + popcornCost;
                }
                else
                {
                    return 14 + popcornCost;
                }
            }
            else
            {
                if (stringDateTime == "Mon" || stringDateTime == "Tue" || stringDateTime == "Wed" || stringDateTime == "Thu")
                {
                    return 8.5 + popcornCost;
                }
                else
                {
                    return 12.5 + popcornCost;
                }
            }
        }
        //ToString():string
        public override string ToString()
        {
            return "Total price of adult ticket is: " + CalculatePrice();
        }
    }
}
