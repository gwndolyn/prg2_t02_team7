﻿//============================================================
// Student Number : S10223182, S10219353
// Student Name : Gwendolyn Leong, Gabriel Koh
// Module Group : T02
//============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG2_T02_Team7
{
    class SeniorCitizen : Ticket
    {
        //yearOfBirth:int
        public int YearOfBirth { get; set; }
        //SeniorCitizen()
        SeniorCitizen() { }
        //SeniorCitizen(Screening, int)
        public SeniorCitizen(Screening screening, int yob) : base(screening)
        {
            YearOfBirth = yob;
        }
        //CalculatePrice():double *
        public override double CalculatePrice()
        {
            string stringDateTime = Screening.ScreeningDateTime.ToString("ddd");
            ///if ((Screening.ScreeningDateTime - Screening.Movie.OpeningDate ).Days <= 7)
            ///{
            ///    return 
           /// }
            if (Screening.ScreeningType == "3D")
            {
                if (stringDateTime == "Mon" || stringDateTime == "Tue" || stringDateTime == "Wed" || stringDateTime == "Thu")
                {
                    return 6;
                }
                else
                {
                    return  14;
                }
            }
            else
            {
                if(stringDateTime == "Mon" || stringDateTime == "Tue" || stringDateTime == "Wed" || stringDateTime == "Thu")
                {
                    return 5;
                }
                else
                {
                    return 12.5;
                }
            }

        }
        //ToString():string
        public override string ToString()
        {
            return "Total price of senior citizen ticket: " + CalculatePrice();
        }
    }
}
