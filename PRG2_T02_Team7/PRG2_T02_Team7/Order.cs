﻿//============================================================
// Student Number : S10223182, S10219353
// Student Name : Gwendolyn Leong, Gabriel Koh
// Module Group : T02
//============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG2_T02_Team7
{
    class Order
    {
        private int orderNo;
        private DateTime orderDateTime;
        private double amount;
        private string status;
        private List<Ticket> ticketList;

        public int OrderNo { get; set; }
        public DateTime OrderDateTime { get; set; }
        public double Amount { get; set; }
        public string Status { get; set; }
        public List<Ticket> TicketList { get; set; } = new List<Ticket>();
        //Order()
        public Order() { }
        //Order(int,DateTime)
        public Order(int orderNo, DateTime orderDateTime)
        {
            OrderNo = orderNo;
            OrderDateTime = orderDateTime;

            Amount = 0;
            Status = "unpaid";
        }
        //AddTicket(Ticket) *
        public void AddTicket(Ticket ticket)
        {
            TicketList.Add(ticket);
        }

        //ToString():String *
        public override string ToString()
        {
            return string.Format("{0,-30} {1,-30} {2,-30} {3,-30}", OrderNo, OrderDateTime, Amount, Status);
        }
    }
}
